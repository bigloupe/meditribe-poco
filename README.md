=======================================================
# MediTribe Poco
=======================================================

### Utilisation de Conan ###

Meditribe-Sesam utilise une libriairie C++ POCO qui doit être disponible sous Linux, MacOS, Windows.
Au lieu de télécharger la librairie et la compiler sur chacun des environnements ,il est possible de partager les librairies compilées avec le projet CONAN.

Ce projet permet de compiler POCO et de l'utiliser ensuite avec Conan dans le projet Meditribe-Sesam

La librairie POCO pour MediTribe doit être recompiler sous Linux en raison de l'option -fPIC manquante dans le package POCO par défaut

Voici les différentes étapes (documentation détaillée dans la documentation de développement de MediTribe)

1// Téléchargement des sources
conan source .

2// Compilation du code
conan build .

3// Mise en package 
mkdir build && cd build
conan package ..

4// Création du package dans le répertoire .conan/data/ pour utilisation par les autres applis
conan export meditribe/stable

La librairie créée s'appelle meditribe/stable au lieu de lasote/stable

Pour passer d'une librairie à l'autre il est nécessaire de changer le fichier conanfile.txt :


    [requires]
    Poco/1.7.3@meditribe/stable